# {{cookiecutter.project_name}}

{{cookiecutter.description}}

## Установка

1. Склонируйте репозиторий: 
```
git clone https://github.com/user/repo.git
```
2. Создаем виртуальное окружение
```
python -m venv venv
venv\Scripts\Activate.ps1
```
3. Обновим pip
```
python -m pip install --upgrade pip
```
4. Установите необходимые пакеты: 
```
pip install -r .\app\requirements\base.txt
```

## Использование


## Лицензия

{{ cookiecutter.open_source_license }}
