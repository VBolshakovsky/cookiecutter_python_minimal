{% if cookiecutter.use_logger == "yes" -%}
import logging
from src.service.logger import init_logger
{% endif -%}

{% if cookiecutter.environment == 'env' -%}
from src.config import Settings as settings
print(settings.APP_NAME)
{% endif -%}

{% if cookiecutter.environment == 'dynaconf' -%}
from dynaconf import settings
print(settings.APP_NAME)
{% endif -%}

{% if cookiecutter.use_logger == "yes" %}
init_logger()
logger = logging.getLogger(settings.APP_NAME)

logger.debug('Это отладочное сообщение.')
logger.info('Это информационное сообщение.')
logger.warning('Это предупреждающее сообщение.')
logger.error('Это сообщение об ошибке.')
logger.critical('Это сообщение о критической ошибке.')
{% endif -%}