# Requirements
python -m venv venv
venv\Scripts\Activate.ps1
. bin/activate

python -m pip install --upgrade pip

pip install -r .\{{ cookiecutter.project_name }}\requirements\base.txt

pip install  -e .\{{ cookiecutter.project_name }}

pip freeze > .\{{ cookiecutter.project_name }}\requirements\prod.txt

pip uninstall -r -y .\{{ cookiecutter.project_name }}\requirements\base.txt
pip uninstall -y {{ cookiecutter.project_name }}

# Docker

docker build --pull --rm -f "{{ cookiecutter.project_name }}\dockerfile" -t ubsgate:latest "{{ cookiecutter.project_name }}"

docker run --dns=8.8.8.8 -t -i {{ cookiecutter.project_name }} /bin/sh

docker compose -f "{{ cookiecutter.project_name }}\docker-compose.yml" up -d --build

docker compose -f "{{ cookiecutter.project_name }}\docker-compose.yml" down

# FASTAPI
cd {{ cookiecutter.project_name }}\src

uvicorn main:app_api --reload
uvicorn main:app_api --reload --port 8000 --host 0.0.0.0 --workers 4

http://127.0.0.1:8000
http://127.0.0.1:8000/docs
http://127.0.0.1:8000/redoc